<?php

namespace Shann\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ShannAdminBundle:Default:index.html.twig');
    }
}
