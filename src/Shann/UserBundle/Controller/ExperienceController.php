<?php

namespace Shann\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Shann\UserBundle\Entity\Responsibility;
use Shann\UserBundle\Entity\Experience;
use Shann\UserBundle\Form\ExperienceType;
use Shann\UserBundle\Entity\User;

class ExperienceController extends Controller
{
	private $route = 'experience';

	/**
	 * List experiences
	 * @return [type] [description]
	 */
	public function indexAction()
	{
		$user = $this->getUser();
		if (!$user instanceof User) {
			return $this->redirectToRoute('security_login');
		}

		$em = $this->getDoctrine()->getManager();
		$experiences = $em->getRepository('ShannUserBundle:Experience')->findAll();

		return $this->render('ShannUserBundle:Experience:index.html.twig', array(
			'route'			=> $this->route,
			'experiences'	=> $experiences,
		));
	}

	/**
	 * New Experience
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function newAction(Request $request)
	{
		$experience = new Experience();

		$form = $this->createForm(ExperienceType::class, $experience);

		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();

			if ($duties = $request->request->get('duties')) {

				foreach ($duties as $duty) {
					$newDuty = new Responsibility();
					$newDuty->setDetail($duty);
					$newDuty->setExperience($experience);
					$em->persist($newDuty);

					$experience->addResponsibility($newDuty);
				}
			}

			$em->persist($experience);
			$em->flush();

			$request->getSession()
				->getFlashBag()
				->add('success', 'New experience successfully added!');

			return $this->redirectToRoute('user_experience_index');
		}

		return $this->render('ShannUserBundle:Experience:form.html.twig', array(
			'form'		=> $form->createView(),
			'route'		=> $this->route,
		));
	}

	public function editAction(Request $request, $id = 0)
	{
		if (!$id) {
			throw new NotFoundHttpException("Invalid ID");
		}

		$em = $this->getDoctrine()->getManager();
		if (!$experience = $em->getRepository('ShannUserBundle:Experience')->findOneById($id)) {
			throw new NotFoundHttpException('Experience Not Found!');
		}

		$form = $this->createForm(ExperienceType::class, $experience);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$experience->getResponsibilities()->clear();

			if ($duties = $request->request->get('duties')) {
				foreach ($duties as $duty) {
					$newDuty = new Responsibility();
					$newDuty->setDetail($duty);
					$newDuty->setExperience($experience);
					$em->persist($newDuty);

					$experience->addResponsibility($newDuty);
				}
			}
			$em->persist($experience);
			$em->flush();
		}


		return $this->render('ShannUserBundle:Experience:form.html.twig', array(
			'form'			=> $form->createView(),
			'route' 		=> $this->route,
			'experience'	=> $experience,
		));
	}
}
